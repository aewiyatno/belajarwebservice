package com.infotama.andika.belajarwebservice;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.infotama.andika.belajarwebservice.Models.Model;
import com.infotama.andika.belajarwebservice.Service.RestApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private TextView idText;
    private TextView namaText;
    private TextView alamatText;
    private TextView statusText;

    public static final String ROOT_URL = "http://103.229.74.80/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        idText = (TextView)findViewById(R.id.text_id);
        namaText = (TextView)findViewById(R.id.text_nama);
        alamatText = (TextView)findViewById(R.id.text_alamat);
        statusText = (TextView)findViewById(R.id.text_status);

        getData();
    }

    private void getData() {
        final ProgressDialog loading = ProgressDialog.show(this, "Fetching data", "Please wait..",
                false, false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RestApi service = retrofit.create(RestApi.class);
        Call<Model> call = service.getUser();

        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                loading.dismiss();
                try {
                    String id = response.body().getUser().getId().toString();
                    String nama = response.body().getUser().getNama();
                    String alamat = response.body().getUser().getAlamat();
                    String status = response.body().getUser().getStatus();

                    idText.setText("ID : " + id);
                    namaText.setText("Nama: " + nama);
                    alamatText.setText("Alamat: " + alamat);
                    statusText.setText("Status: " + status);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {

            }
        });
    }
}
