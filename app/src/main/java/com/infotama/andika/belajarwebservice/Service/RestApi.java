package com.infotama.andika.belajarwebservice.Service;

import com.infotama.andika.belajarwebservice.Models.Model;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Andika on 09/09/2016.
 */
public interface RestApi {
    @GET("/")
    Call<Model> getUser();
}
